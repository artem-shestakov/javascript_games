import Phaser from 'phaser';

class PlayGame extends Phaser.Scene {
    constructor() {
        super('PlayGame');
    }

    create() {
        this.gameSpeed = 5;
        const { height, width} = this.game.config;

        this.ground = this.add.tileSprite(0, height, width, 26, 'ground').setOrigin(0, 1);
        this.dino = this.physics.add.sprite(0, height, 'dino').setOrigin(0, 1).setCollideWorldBounds(true).setGravityY(5000);

        this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

        this.handleInputs();
 
    }

    handleInputs() {
        this.spacebar.on('down', (key, event) => {
            this.dino.setVelocityY(-1600);
        })
    }

    update() {
        this.ground.tilePositionX += this.gameSpeed;
    }
}

export default PlayGame;