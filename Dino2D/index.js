import Phaser from 'phaser';
import LoadGame from './LoadGame';
import PlayGame from './PlayGame';

var config = {
        type: Phaser.AUTO,
        width: 1000,
        height: 340,
        backgroundColor: 0xffffff,
        physics: {
            default: 'arcade',
            arcade: {
                debug: true
            }
        },
        scene: [LoadGame, PlayGame]
    };

let game = new Phaser.Game(config);
