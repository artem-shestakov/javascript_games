import Phaser from 'phaser';

class LoadGame extends Phaser.Scene {
    constructor() {
        super('LoadGame');
    }

    preload() {
        this.load.image('ground', 'assets/ground.png')
        this.load.image('dino', 'assets/dino.png')
    }

    create() {
        this.scene.start('PlayGame')
    }
}

export default LoadGame;